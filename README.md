+++++++++++ secubetm-libraries +++++++++++

Host libraries for SEcube

Compilation:
- mkdir build
- cd build
- cmake ..
- make


Install
- sudo make install (optional)

shared library is copied to /usr/lib/secubetm-libraries/
header files are copied to /usr/include/secubetm-libraries/
